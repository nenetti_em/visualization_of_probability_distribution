#! /usr/bin/env python
import threading
import time

import actionlib
import numpy as np
import rospy
import sensor_msgs.msg as sensor_msgs
import visualization_msgs.msg as visualization_msgs

import std_msgs.msg as std_msgs
import geometry_msgs.msg as geometry_msgs
from pointcloud2_encoder import PointCloud2Encoder
from modules import AnimationDistribution


class Visualizer:

    def __init__(self):
        # resolution = rospy.get_param("~resolution")
        # self._is_auto_subscribe = rospy.get_param("~is_auto_subscribe")
        #
        # min_x = rospy.get_param("~min_x", default=-np.inf)
        # min_y = rospy.get_param("~min_y", default=-np.inf)
        # min_z = rospy.get_param("~min_z", default=-np.inf)
        # max_x = rospy.get_param("~max_x", default=np.inf)
        # max_y = rospy.get_param("~max_y", default=np.inf)
        # max_z = rospy.get_param("~max_z", default=np.inf)
        #
        # self.min_point = (min_x, min_y, min_z)
        # self.max_point = (max_x, max_y, max_z)
        # self._octomap = OctoMap(resolution=resolution)
        #
        # self._is_rgb_publish_processing = False
        # self._is_label_publish_processing = False
        #
        # self._is_callback_registered = False
        #
        # self._subscriber_kwargs = dict(
        #     name="/input/image/compressed",
        #     data_class=sensor_msgs.PointCloud2,
        #     callback=self._distribution_callback
        # )
        # self._subscriber = rospy.Subscriber()
        #
        # # Publisher
        self._publisher = rospy.Publisher("/dist", sensor_msgs.PointCloud2, queue_size=1)
        time.sleep(1)
        self._distributions = []

    # ==================================================================================================
    #
    #   Main Method
    #
    # ==================================================================================================
    def main(self):
        while not rospy.is_shutdown():
            if 0 < len(self._distributions):
                xyzs = []
                rgbs = []
                for distribution in self._distributions:
                    distribution.update()
                    xyz = distribution.get_xyz()
                    rgb = xyz[..., 2] / np.max(xyz[..., 2])
                    rgb *= 255
                    rgb = np.stack([rgb, rgb, rgb], axis=2)
                    print(xyz.shape)

                    xyzs.append(xyz.reshape(-1, 3))
                    rgbs.append(rgb.reshape(-1, 3))

                xyz = np.concatenate(xyzs, axis=0)
                rgb = np.concatenate(rgbs, axis=0)
                print(xyz.shape, rgb.shape)
                distribution = PointCloud2Encoder.xyz_rgb_to_pointcloud2(xyz, rgb, rospy.Time.now(), "map")
                self._publisher.publish(distribution)
            rospy.sleep(0.1)
            # connection_exists = (self._detection_image_publisher.get_num_connections() > 0) or (
            #         self._bounding_boxes_publisher.get_num_connections() > 0)
            # if connection_exists and (not self._is_callback_registered):
            #     self._subscriber = rospy.Subscriber(*self._subscriber_args)
            #     self._is_callback_registered = True
            #     rospy.loginfo(f"Register subscriber: {self._subscriber.resolved_name}")
            #
            # elif (not connection_exists) and self._is_callback_registered:
            #     rospy.loginfo(f"Unregister subscriber: {self._subscriber.resolved_name}")
            #     self._subscriber.unregister()
            #     self._subscriber = None
            #     self._is_callback_registered = False

    # ==================================================================================================
    #
    #   ROS Subscriber Callback
    #
    # ==================================================================================================
    def _distribution_callback(self, msg):
        """
        Args:
            msg ():
        """
        x = np.random.random(100) * (0.5 + np.random.random() / 2) + np.random.randint(-3, 3)
        y = np.random.random(100) * (0.5 + np.random.random() / 2) + np.random.randint(-3, 3)
        mu_x = np.mean(x)
        mu_y = np.mean(y)
        sigma_x = np.std(x)
        sigma_y = np.std(y)
        covariance = (np.sum(x * y) / len(x)) - (mu_x * mu_y)

        cloud = self._make_cloud(mu_x, mu_y, sigma_x, sigma_y, covariance)
        self._distributions.append(cloud)
        # sensor_msgs.PointCloud2()

    # ==================================================================================================
    #
    #   ROS Action Callback
    #
    # ==================================================================================================
    def _observation_action_callback(self, goal):
        """
        Args:
            goal (em_octomap_msgs.ObservationOnceGoal):
        """
        rospy.loginfo(f"Receive: {self._generate_server.action_server.ns}")
        topic, topic_type = self._pointcloud2_subscriber_kargs["name"], self._pointcloud2_subscriber_kargs["data_class"]

        cloud = rospy.wait_for_message(topic=topic, topic_type=topic_type)

        self._update_map(cloud)

        result = em_octomap_msgs.ObservationOnceResult()
        result.id = goal.id
        self._generate_server.set_succeeded(result)
        rospy.loginfo(f"Succeeded: {self._generate_server.action_server.ns}")

    def _update_octomap_action_callback(self, goal):
        """
        Args:
            goal (em_octomap_msgs.UpdateOctoMapGoal):
        """
        rospy.loginfo(f"Receive: {self._update_map_server.action_server.ns}")
        self._update_map(goal.cloud)
        result = em_octomap_msgs.UpdateOctoMapResult()
        result.id = goal.id
        self._update_map_server.set_succeeded(result)
        rospy.loginfo(f"Succeeded: {self._update_map_server.action_server.ns}")

    def _request_cloud_octomap_action_callback(self, goal):
        """
        Args:
            goal (yolo_ros_msgs.CheckForObjectsGoal):
        """
        rospy.loginfo(f"Receive: {self._cloud_octomap_server.action_server.ns}")

        leaves = self._octomap.get_all_leaves()
        xyzs = self._octomap.leaves_to_xyz_array(leaves)
        rgbs = self._octomap.leaves_to_rgb_array(leaves)
        labels = self._octomap.leaves_to_label_array(leaves)

        cloud = PointCloud2Encoder.xyz_rgb_label_to_pointcloud2(
            xyzs, rgbs, labels, stamp=rospy.Time.now(), frame_id="map"
        )

        result = em_octomap_msgs.RequestCloudOctoMapResult()
        result.id = goal.id
        result.cloud = cloud
        self._cloud_octomap_server.set_succeeded(result)
        rospy.loginfo(f"Succeeded: {self._cloud_octomap_server.action_server.ns}")

    def _reset_action_callback(self, goal):
        """
        Args:
            goal (em_octomap_msgs.ResetGoal):
        """
        rospy.loginfo(f"Receive: {self._reset_server.action_server.ns}")

        if goal.reset_area:
            self.min_point = self._point_to_tuple(goal.min_point)
            self.max_point = self._point_to_tuple(goal.max_point)

        if goal.reset_resolution:
            self._octomap.reset_resolution(goal.resolution)

        self._reset_map()

        result = em_octomap_msgs.ResetResult(goal.id)
        self._reset_server.set_succeeded(result)
        rospy.loginfo(f"Succeeded: {self._reset_server.action_server.ns}")

    # ==================================================================================================
    #
    #   ROS Publisher
    #
    # ==================================================================================================

    # ==================================================================================================
    #
    #   Static Method
    #
    # ==================================================================================================
    def _make_cloud(self, mu_x, mu_y, sigma_x, sigma_y, covariance):
        sigma_x_y = sigma_x * sigma_y
        rho = covariance / sigma_x_y

        x, y = self.visualize_range(mu_x, mu_y, sigma_x, sigma_y, 0.01)

        h = 1 / (2 * np.pi * sigma_x_y * np.sqrt(1 - rho ** 2))
        g = -1 / (2 * (1 - rho ** 2))
        sigma_x2 = sigma_x ** 2
        sigma_y2 = sigma_y ** 2

        ex = ((x - mu_x) ** 2) / sigma_x2
        ey = ((y - mu_y) ** 2) / sigma_y2
        exy = (2 * rho * (x - mu_x) * (y - mu_y)) / sigma_x_y

        z = h * np.exp(g * (ex + ey - exy))
        if np.max(z) > 1.0:
            z /= np.max(z)
        xyz = np.stack([x, y, z], axis=2)
        return AnimationDistribution(xyz, time=30)

    @staticmethod
    def visualize_range(mu_x, mu_y, sigma_x, sigma_y, resolution):
        min_x = (mu_x - 3 * sigma_x)
        max_x = (mu_x + 3 * sigma_x)

        min_y = (mu_y - 3 * sigma_y)
        max_y = (mu_y + 3 * sigma_y)

        resolution = max(max_x - min_x, max_y - min_y) / 100

        x = np.arange(min_x, max_x, resolution)
        y = np.arange(min_y, max_y, resolution)
        x, y = np.meshgrid(x, y)
        return x, y


if __name__ == "__main__":
    rospy.init_node("visualization_of_probability_distribution")
    visualizer = Visualizer()
    for i in range(10):
        visualizer._distribution_callback(None)
    visualizer.main()

    # Visualizer().main()
