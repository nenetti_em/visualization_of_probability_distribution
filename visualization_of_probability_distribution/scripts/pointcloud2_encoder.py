import numpy as np
import genpy
import ros_numpy
import rospy
from cv_bridge import CvBridge
from ros_numpy.point_cloud2 import array_to_pointcloud2
import sensor_msgs.msg as sensor_msgs


class PointCloud2Encoder:
    _cv_bridge = CvBridge()

    # ==================================================================================================
    #
    #   Public Method
    #
    # ==================================================================================================
    @classmethod
    def xyz_to_pointcloud2(cls, points, stamp, frame_id):
        """
        Args:
            points (np.ndarray):
            stamp (genpy.Time):
            frame_id (str):

        Returns:
            sensor_msgs.PointCloud2:
        """
        cloud_array = cls._xyz_to_cloud_format(points)
        return array_to_pointcloud2(cloud_array, stamp, frame_id)

    @classmethod
    def xyz_rgb_to_pointcloud2(cls, xyz, rgb, stamp, frame_id):
        """
        Args:
            xyz (np.ndarray):
            rgb (np.ndarray):
            stamp (genpy.Time):
            frame_id (str):

        Returns:
            sensor_msgs.PointCloud2:
        """
        cloud_array = cls._xyz_rgb_to_cloud_format(xyz, rgb)
        return array_to_pointcloud2(cloud_array, stamp, frame_id)

    @classmethod
    def xyz_rgb_label_to_pointcloud2(cls, xyz, rgb, label, stamp, frame_id):
        """
        Args:
            xyz (np.ndarray):
            rgb (np.ndarray):
            label (np.ndarray):
            stamp (genpy.Time):
            frame_id (str):

        Returns:
            sensor_msgs.PointCloud2:
        """
        cloud_array = cls._xyz_rgb_label_to_cloud_format(xyz, rgb, label)
        return array_to_pointcloud2(cloud_array, stamp, frame_id)

    @classmethod
    def rgbd_image_msgs_to_pointcloud2(cls, camera_info, image_msg, depth_msg):
        """
        Args:
            camera_info (sensor_msgs.CameraInfo):
            image_msg (sensor_msgs.CompressedImage):
            depth_msg (sensor_msgs.Image):
        """
        bgr8 = cls._cv_bridge.compressed_imgmsg_to_cv2(image_msg)
        depth = cls._cv_bridge.imgmsg_to_cv2(depth_msg)
        rgb_f32 = cls._rgb_to_cloud_format(rgb8=bgr8[..., ::-1])

        p = camera_info.P
        x, y, z = cls._depth_to_xyz(depth, p)

        cloud_dtype = [("x", np.float32), ("y", np.float32), ("z", np.float32), ("rgb", np.float32)]
        cloud = np.zeros((depth_msg.height, depth_msg.width), cloud_dtype)
        cloud["x"] = x
        cloud["y"] = y
        cloud["z"] = z
        cloud["rgb"] = rgb_f32

        return ros_numpy.point_cloud2.array_to_pointcloud2(cloud, stamp=rospy.Time.now(), frame_id=depth_msg.header.frame_id)

    @classmethod
    def depth_to_xyz(cls, camera_info, depth):
        """
        Args:
            camera_info (sensor_msgs.CameraInfo):
            depth (np.ndarray):
        """
        p = camera_info.P
        x, y, z = cls._depth_to_xyz(depth, p)
        xyz = np.empty((*depth.shape, 3), dtype=np.float32)
        xyz[..., 0] = x
        xyz[..., 1] = y
        xyz[..., 2] = z

        return xyz

    # ==================================================================================================
    #
    #   Private Method
    #
    # ==================================================================================================
    @classmethod
    def _xyz_to_cloud_format(cls, xyz):
        xyz = cls._reshape_pointcloud2_format(xyz)
        height, width = xyz.shape[:2]
        cloud_dtype = [("x", np.float32), ("y", np.float32), ("z", np.float32)]
        cloud = np.empty((height, width), cloud_dtype)
        cloud["x"] = xyz[:, :, 0]
        cloud["y"] = xyz[:, :, 1]
        cloud["z"] = xyz[:, :, 2]

        return cloud

    @classmethod
    def _xyz_rgb_to_cloud_format(cls, xyz, rgb):
        xyz = cls._reshape_pointcloud2_format(xyz)
        rgb = cls._reshape_pointcloud2_format(rgb)

        height, width = xyz.shape[:2]
        cloud_dtype = [("x", np.float32), ("y", np.float32), ("z", np.float32), ("rgb", np.float32)]
        cloud = np.empty((height, width), cloud_dtype)
        cloud["x"] = xyz[:, :, 0]
        cloud["y"] = xyz[:, :, 1]
        cloud["z"] = xyz[:, :, 2]

        rgb = rgb.astype(np.uint32)
        r, g, b = (rgb[:, :, 0], rgb[:, :, 1], rgb[:, :, 2])
        rgb_f32 = np.array((r << 0) | (g << 8) | (b << 16), dtype=np.uint32)
        rgb_f32.dtype = np.float32
        cloud["rgb"] = rgb_f32

        return cloud

    @classmethod
    def _xyz_rgb_label_to_cloud_format(cls, xyz, rgb, label):
        """
        Args:
            xyz (np.ndarray):
            rgb (np.ndarray):
            label (np.ndarray):

        Returns:
            np.ndarray:
        """
        xyz = cls._reshape_pointcloud2_format(xyz)
        rgb = cls._reshape_pointcloud2_format(rgb)

        height, width = xyz.shape[:2]
        cloud_dtype = [("x", np.float32), ("y", np.float32), ("z", np.float32), ("rgb", np.float32), ("label", np.uint32)]
        cloud = np.empty((height, width), cloud_dtype)
        cloud["x"] = xyz[:, :, 0]
        cloud["y"] = xyz[:, :, 1]
        cloud["z"] = xyz[:, :, 2]

        rgb = rgb.astype(np.uint32)
        r, g, b = (rgb[:, :, 0], rgb[:, :, 1], rgb[:, :, 2])
        rgb_f32 = np.array((r << 0) | (g << 8) | (b << 16), dtype=np.uint32)
        rgb_f32.dtype = np.float32
        cloud["rgb"] = rgb_f32

        cloud["label"] = label

        return cloud

    @staticmethod
    def _reshape_pointcloud2_format(points):
        if points.ndim != 3:
            return points.reshape(1, -1, 3)
        return points

    @staticmethod
    def _depth_to_xyz(z, p):
        """
        Args:
            z (np.ndarray):
            p (tuple):

        Returns:
            (np.ndarray, np.ndarray, np.ndarray):
        """
        fx, fy = p[0], p[5]
        cx, cy = p[2], p[6]

        u = np.tile(np.arange(640), (480, 1))
        v = np.tile(np.arange(480), (640, 1)).T

        x = (u - cx) * z / fx
        y = (v - cy) * z / fy

        return x, y, z

    @staticmethod
    def _rgb_to_cloud_format(rgb8):
        """
        Args:
            rgb8 (np.ndarray):

        Returns:
            np.ndarray:
        """
        rgb8 = rgb8.astype(np.uint32)
        r, g, b = rgb8[..., 0], rgb8[..., 1], rgb8[..., 2]

        rgb = np.array((r << 16) | (g << 8) | (b << 0), dtype=np.uint32)
        rgb.dtype = np.float32

        return rgb
