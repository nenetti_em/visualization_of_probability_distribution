import numpy as np


class AnimationDistribution:

    def __init__(self, xyz, time=30):
        self._xyz = xyz
        self._per_xyz = np.copy(xyz)
        self._per_xyz[..., 2] /= time
        self._animation_xyz = np.copy(xyz)

        self._now_time = 0
        self._target_time = time

    def update(self):
        if self._now_time < self._target_time:
            self._now_time += 1

    def get_xyz(self):
        if self._now_time > self._target_time:
            return self._xyz
        else:
            self._animation_xyz[..., 2] = self._per_xyz[..., 2] * self._now_time
            return self._animation_xyz
